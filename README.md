# Dkoch

Dkoch is a program for learning morse code using the Koch and/or Farnsworth methods.

![Screenshot](screenshots/screenshot.png)

## Usage

    dkoch lesson [options] [preset_lesson_# (1-40)]
    dkoch test [options] [preset_test_# (1-40)]
    dkoch text [options] [text]
    
Getting Help:
    
    dkoch lesson --help
    dkoch test --help
    dkoch text --help
    
Various options can be used to control the speed of characters, and the spacing between characters and words. See the help for more options.

    --wpm              Words per minute. (default: 20)
    --char-spacing-wpm Effective wpm for spaces between characters.
    --word-spacing-wpm Effective wpm for spaces between words.
    --spacing-wpm Sets both char-spacing-wpm and word-spacing-wpm.
    
## Examples
    
Listen to the first lesson:
    
    dkoch lesson 1
    
Take the first test:
    
    dkoch test 1
    
Play some text in morse code:
    
    dkoch text "The quick brown fox jumps over the lazy dog"

## Building

This program is written in the [D Language](https://dlang.org). It requires [SDL](https://www.libsdl.org), a D compiler, and [DUB](https://github.com/dlang/dub). It was written using Linux, and has not been tested on other platforms. It can be compiled with the following command:
    

    dub build --build=release
