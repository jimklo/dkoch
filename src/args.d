module args;

import std.exception : enforce;

enum Command
{
        test,
        lesson,
        text,
        listdevices
};

private struct Test
{
        string groupChars;
        string highFreqChars;
};

private Test[] tests;

static this()
{
        auto order = "kmuresnaptlwi.jz=foy,vg5/q92h38b?47c1d60x";
        foreach(i; 1..order.length) {
                tests ~= Test(order[0..(i+1)], order[i..i+1]);
        }
}

struct CommonArguments
{
        int    toneHz         = 750;

        float  wpm            = 20;
        float  charSpacingWpm = float.nan;
        float  wordSpacingWpm = float.nan;

        float  riseTimeMs     = 5;

        float  startDelay     = 0.2;
        // Sometimes the audio gets cut off a bit without this.
        float  endDelay       = 0.2;

        bool   noColour       = false;

        string outputFile     = "";

        version(sdl_output) {
                string sdlDeviceName = "";
        }
};

struct TestArguments
{
        int testTime         = 180;
        bool quiet           = false;

        // Code group generation
        string groupChars    = "";
        string highFreqChars = "";
        int highFreqPercent  = 20;
        int groupLengthMin   = 5;
        int groupLengthMax   = 5;

        string wordListFile  = "";
};

struct LessonArguments
{
        string chars = "";
};

struct TextArguments
{
        string text = "";
};

struct MetaArguments
{
        // Common
        float  spacingWpm = float.nan;

        // Test
        int test   = 0;

        // Lesson
        int lesson = 0;

        // Text
        string file = "";
};

public struct Arguments
{
        Command command;

        CommonArguments common;
        TestArguments   test;
        LessonArguments lesson;
        TextArguments   text;
        MetaArguments   meta;
};

string getExeName()
{
        import std.file : thisExePath;
        import std.path : baseName;

        auto exe = thisExePath();
        exe = baseName(exe);

        return exe;
}

void printCommandUsage(bool error)
{
        import std.stdio  : stdout, stderr, writefln;
        import std.traits : EnumMembers;

        auto f = error? stderr : stdout;

        auto exe = getExeName();

        f.writefln("Usage: %s [-h] <command> [options] [args]", exe);
        f.writefln("For help with a specific command: %s <command> -h", exe);
        f.writeln("\nAvailable commands:");
        foreach(immutable cmd; [EnumMembers!Command]) {
                writefln("\t%s", cmd);
        }
}

bool getCommand(Arguments* argsOut, ref string[] argsIn)
{
        import std.stdio : stderr;
        import std.conv : to;
        import std.algorithm : remove;

        if(argsIn.length < 2) {
                printCommandUsage(true);
                return false;
        }

        if(argsIn[1] == "-h" || argsIn[1] == "--help") {
                printCommandUsage(false);
                return false;
        }

        try {
                argsOut.command = to!Command(argsIn[1]);
                argsIn = argsIn.remove(1);
        } catch(Exception e) {
                stderr.writefln("Invalid command: \"%s\". Type %s -h for usage.",
                                argsIn[1], getExeName());
                return false;
        }

        return true;
}

bool getOptions(Arguments* argsOut, string[] argsIn)
{
        import std.getopt;
        import std.conv      : to;
        import std.algorithm : remove;
        import std.string    : format;
        import std.typecons  : tuple;

        auto pre = tuple(
                std.getopt.config.caseSensitive,
        );

        auto testArgs = tuple(
                "test-time",
                format("Test time, in seconds. (default: %s)",
                       argsOut.test.testTime),
                &argsOut.test.testTime,

                "quiet",
                "Don't print instructions.",
                &argsOut.test.quiet,

                "group-chars", "Characters to include in code groups.",
                &argsOut.test.groupChars,

                "high-freq-chars", "Characters to output at a higher frequency.",
                &argsOut.test.highFreqChars,

                "high-freq-percent", format(
                        "This percentage of characters will be selected only " ~
                        "from the high frequency group. (default %s)",
                        argsOut.test.highFreqPercent
                ), &argsOut.test.highFreqPercent,

                "group-length-min",
                format("Minimum length of each code group. (default: %s)",
                       argsOut.test.groupLengthMin),
                &argsOut.test.groupLengthMin,

                "group-length-max",
                format("Maximum length of each code group. (default: %s)",
                       argsOut.test.groupLengthMax),
                &argsOut.test.groupLengthMax,

                "word-list", "File containing a list of words to use " ~
                             "instead of random code groups",
                &argsOut.test.wordListFile,
        );

        auto lessonArgs = tuple(
                "chars", "Characters to include in lesson.",
                &argsOut.lesson.chars,
        );

        auto textArgs = tuple(
                "file",
                "Read text from file and output with the current settings",
                &argsOut.meta.file,
        );

        auto commonArgs = tuple(

                "tone", 
                format("Morse code tone in hz. (default: %s)",
                       argsOut.common.toneHz),
                &argsOut.common.toneHz,

                "wpm", format("Words per minute. (default: %s)",
                              argsOut.common.wpm),
                &argsOut.common.wpm,

                "char-spacing-wpm",
                "Effective wpm for spaces between characters.",
                &argsOut.common.charSpacingWpm,

                "word-spacing-wpm",
                "Effective wpm for spaces between words.",
                &argsOut.common.wordSpacingWpm,

                "rise-time",
                format("Waveform rise / fall time (milliseconds). (default: %s)",
                       argsOut.common.riseTimeMs),
                &argsOut.common.riseTimeMs,

                "spacing-wpm",
                "Sets both char-spacing-wpm and word-spacing-wpm.",
                &argsOut.meta.spacingWpm,

                "start-delay",
                format("Delay before outputting first code (seconds). " ~
                       "(default: %s)", argsOut.common.startDelay),
                &argsOut.common.startDelay,

                "end-delay",
                format("Delay before exiting program (seconds). " ~
                       "(default: %s)", argsOut.common.endDelay),
                &argsOut.common.endDelay,

                "no-colour", "Don't use colours in test results.",
                &argsOut.common.noColour,

                "output-file",
                "Output audio to raw PCM file (44.1khz 16-bit signed data).",
                &argsOut.common.outputFile,
        );

        version(sdl_output) {
                auto outputArgs = tuple(
                        "sdl-device-name",
                        "Output to specific SDL device",
                        &argsOut.common.sdlDeviceName,
                );
        } else {
                auto outputArgs = tuple();
        }


        string usage = format("usage: %s %s [options]",
                        getExeName, to!string(argsOut.command));

        // This is up here because some -- options, such as --file for the text
        // command mean that no non-hyphenated arguments are needed.
        bool noArgs = argsIn.length == 1;

        auto post = tuple(commonArgs[], outputArgs[]);

        GetoptResult helpInformation;

        final switch(argsOut.command) {
                case Command.test:
                        usage ~= format(" [preset_test_# (%s-%s)]",
                                        1, tests.length);
                        helpInformation = getopt(
                                argsIn, pre[], testArgs[], post[]);
                        break;
                case Command.lesson:
                        usage ~= format(" [preset_lesson_# (%s-%s)]",
                                        1, tests.length);
                        helpInformation = getopt(
                                argsIn, pre[], lessonArgs[], post[]);
                        break;
                case Command.text:
                        usage ~= " [text]";
                        helpInformation = getopt(
                                argsIn, pre[], textArgs[], post[]);
                        break;
                case Command.listdevices:
                        helpInformation = getopt(argsIn, pre[]);
                        noArgs = false;
                        break;
        }

        if(noArgs || helpInformation.helpWanted) {
                defaultGetoptPrinter(usage, helpInformation.options);
                return false;
        }

        final switch(argsOut.command) {
                case Command.test:
                        if(argsIn.length > 1) {
                                argsOut.meta.test = to!int(argsIn[1]);
                                if(argsOut.meta.test == 0) {
                                        // This will trigger an error message
                                        // if the user explicitly requested 0
                                        argsOut.meta.test = -1;
                                }
                                argsIn = argsIn.remove(1);
                        }
                        break;
                case Command.lesson:
                        if(argsIn.length > 1) {
                                argsOut.meta.lesson = to!int(argsIn[1]);
                                if(argsOut.meta.lesson == 0) {
                                        // This will trigger an error message
                                        // if the user explicitly requested 0
                                        argsOut.meta.lesson = -1;
                                }
                                argsIn = argsIn.remove(1);
                        }
                        break;
                case Command.text:
                        if(argsIn.length > 1) {
                                argsOut.text.text = argsIn[1];
                                argsIn = argsIn.remove(1);
                        }
                        break;
                case Command.listdevices:
                        break;
        }

        enforce(argsIn.length <= 1, 
                format("Unrecognized argument: '%s'", argsIn[1]));

        return true;
}

void expandMetaOptions(Arguments* argsOut)
{
        import std.string    : format;
        import std.algorithm : canFind;
        import std.file      : readText;
        import std.math      : isNaN;

        with(argsOut) {

                enforce(meta.test >= 0 && meta.test <= tests.length,
                        format("Test must be between 1 and %s", tests.length));

                if(meta.test >= 1) {
                        test.groupChars = tests[meta.test-1].groupChars;
                        test.highFreqChars = tests[meta.test-1].highFreqChars;
                }

                enforce(meta.lesson >= 0 && meta.lesson <= tests.length,
                        format("Lesson must be between 1 and %s", tests.length));

                if(meta.lesson >= 1) {

                        // If doing a lesson, find the characters in the
                        // selected test that were not in the previous one.

                        string current = tests[meta.lesson-1].groupChars;
                        string previous = meta.lesson == 1?
                               "" : tests[meta.lesson-2].groupChars;

                        string newChars = "";
                        foreach(c; current) {
                                if(!previous.canFind(c)) {
                                        newChars ~= c;
                                }
                        }

                        argsOut.lesson.chars = newChars;
                }

                if(meta.file != "") {
                        text.text = meta.file.readText();
                }

                if(!isNaN(meta.spacingWpm)) {
                        common.wordSpacingWpm = meta.spacingWpm;
                        common.charSpacingWpm = meta.spacingWpm;
                }

                // Not exactly meta options, but useful to expand them here:
                if(isNaN(common.wordSpacingWpm)) {
                        common.wordSpacingWpm = common.wpm;
                }

                if(isNaN(common.charSpacingWpm)) {
                        common.charSpacingWpm = common.wpm;
                }
        }
}

bool allCharsValid(string chars)
{
        import morse_alphabet : alphabet;

        foreach(c; chars) {
                if(c !in alphabet) {
                        return false;
                }
        }

        return true;
}

void validateArgs(Arguments* args)
{
        import std.algorithm : canFind;

        // Common options

        auto common = &args.common;

        enforce(common.toneHz >= 100 && common.toneHz <= 3000,
                "Tone must be between 100 and 3000.");

        enforce(common.wpm >= 1 && common.wpm <= 200,
                "wpm must be between 1 and 200.");

        enforce(common.charSpacingWpm >= 1 &&
                common.charSpacingWpm <= 200,
                "wpm must be between 1 and 200.");

        enforce(common.wordSpacingWpm >= 1 &&
                common.wordSpacingWpm <= 200,
                "wpm must be between 1 and 200.");

        enforce(common.riseTimeMs >= 0.0f && common.riseTimeMs <= 20.0f,
                "Rise time must be between 0 and 20");

        enforce(common.startDelay >= 0.0f,
                "Start delay must be >= 0.");

        // Test command

        if(args.command == Command.test) {

                auto test = &args.test;

                enforce(test.testTime >= 0 && test.testTime <= 1200,
                        "Test time must be between 0 and 1200");

                enforce(test.groupChars != "" || test.wordListFile != "",
                        "You must specify one of: --group-chars --word-list " ~
                        "or [preset_test_#].");

                if(test.highFreqChars != "") {
                        foreach(c; test.highFreqChars) {
                                enforce(test.groupChars.canFind(c),
                                        "All high frequency chars must also " ~
                                        "be specified as group chars");
                        }
                }

                enforce(test.highFreqPercent >= 0 &&
                        test.highFreqPercent <= 100,
                        "High frequency percent must be between 0 and 100.");

                enforce(allCharsValid(test.groupChars),
                        "Invalid morse code chars specified for test.");

                enforce(test.groupLengthMin > 0 &&
                        test.groupLengthMin < 50,
                        "Group minimum length must be between 0 and 50.");

                enforce(test.groupLengthMax > 0
                        && test.groupLengthMax < 50,
                        "Group maximum length must be between 0 and 50.");

                enforce(test.groupLengthMax >= test.groupLengthMin,
                        "Group length maximum must be greater than minimum.");

                enforce(args.meta.test == 0 || test.wordListFile == "",
                        "Word list file cannot be used with preset tests.");

                import std.file   : exists;
                import std.string : format;

                enforce(test.wordListFile == "" || exists(test.wordListFile),
                        format("File '%s' does not exist.", test.wordListFile));
        }

        if(args.command == Command.lesson) {

                auto lesson = &args.lesson;

                enforce(lesson.chars.length > 0,
                        "Lesson chars must not be empty.");

                enforce(allCharsValid(lesson.chars),
                        "Invalid morse code chars specified for lesson.");
        }
}

Arguments* parseArguments(string[] argsIn)
{
        import std.stdio : stderr, writeln;

        Arguments* argsOut = new Arguments;

        try {

                if(!getCommand(argsOut, argsIn)) {
                        return null;
                }

                if(!getOptions(argsOut, argsIn)) {
                        return null;
                }

                expandMetaOptions(argsOut);
                validateArgs(argsOut);

        } catch(Exception e) {
                stderr.writeln("Error parsing command-line:\n\t", e.msg);
                return null;
        }

        return argsOut;
}
