module basic_output;

import std.exception   : enforce;
import output          : Output;
import core.sync.mutex : Mutex;

abstract class BasicOutput : Output
{
    public:
        this(int sampRate)
        {
                _sampRate = sampRate;
                _mutex = new Mutex();
        }

        override int addSound(immutable(float)[]  data)
        {
                synchronized(_mutex) {
                        while(_nextId in _sounds) {
                                _nextId++;
                        }

                        int id = _nextId;
                        _nextId++;

                        _sounds[id] = data;

                        return id;
                }
        }

        override void queueSound(int id)
        {
                synchronized(_mutex) {
                        enforce(id in _sounds, "Invalid sound id.");
                        _queue ~= id;
                }
        }

        override int getSampleRate()
        {
                synchronized(_mutex) {
                        return _sampRate;
                }
        }

        ulong queueLength()
        {
                synchronized(_mutex) {
                        return _queue.length;
                }
        }

        int queueFront()
        {
                synchronized(_mutex) {
                        return _queue[0];
                }
        }

        void popQueue()
        {
                synchronized(_mutex) {
                        _queue = _queue[1..$];
                }
        }

        immutable(float)[] getSound(int id)
        {
                synchronized(_mutex) {
                        return _sounds[id];
                }
        }

    protected:
        Mutex             _mutex;

    private:
        immutable int           _sampRate;

        int[]                   _queue;
        immutable(float)[][int] _sounds;
        int                     _nextId = 0;
};


