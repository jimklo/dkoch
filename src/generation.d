module generation;

import std.exception : assumeUnique;

immutable(float)[]
generateTone(int sampRate, float freq, float duration, float riseTime)
{
        import std.math : sin, cos, PI;

        float[] tone;
        int count = cast(int)(sampRate * duration);
        tone.reserve(count);

        float mult = 1.0f / sampRate * 2.0f * PI * freq;

        foreach(i; 0..count) {
                tone ~= sin(float(i) * mult);
        }

        auto end = cast(int)(riseTime * sampRate);
        if(end > (count/2)) {
                end = count/2;
        }
        foreach(i; 0..end) {
                float amt = cast(float)i / end;
                // This changes it from a linear rise / fall to following 
                // the curve of a cosine.
                amt = (cos((1.0f - amt) * PI) + 1.0f) / 2.0f;
                tone[i] *= amt;
                tone[count-1-i] *= amt;
        }

        return assumeUnique(tone);
}

immutable(float)[]
generateSpace(int sampRate, float duration)
{
        int count = cast(int)(sampRate * duration);
        float[] space = new float[count];
        space[0..count] = 0;

        return assumeUnique(space);
}
