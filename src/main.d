import morse       : Morse;
import output      : Output;
import args        : parseArguments, Arguments, Command;
import test        : doTest;
import file_output : FileOutput;

version(sdl_output) {
    pragma(msg, "Using SDL output.");
} else version(dummy_output) {
    pragma(msg, "Using Dummy output.");
} else {
    static assert(false, "No output method selected.");
}

void doLesson(Output output, Morse morse, Arguments* args)
{
        import std.stdio : writefln;

        int charCount = 5;
        foreach(c; args.lesson.chars) {
                string s;
                writefln("Character: %s", c);
                for(int k = 0; k < charCount; ++k) {
                        s ~= c ~ " ";
                }
                morse.outputText(s);
                output.startPlayback();
                output.waitUntilFinished();
        }
}

void doText(Output output, Morse morse, Arguments* args)
{
        morse.outputText(args.text.text);
        output.startPlayback();
        output.waitUntilFinished();
}

void doListDevices()
{
        version(sdl_output) {
                import sdl_output : SdlOutput;
                import std.stdio : writefln;
                auto names = SdlOutput.getDeviceNames();
                writefln("SDL device names: ");
                foreach(n; names) {
                        writefln("    %s", n);
                }
        }
}

int main(string[] argsIn)
{
        auto args = parseArguments(argsIn);

        if(args == null) {
                return 1;
        }

        if(args.command == Command.listdevices) {
                doListDevices();
                return 0;
        }

        Output output;

        if(args.common.outputFile != "") {
                output = new FileOutput(args.common.outputFile);
        } else {
                version(sdl_output) {
                        import sdl_output : SdlOutput;
                        output = new SdlOutput(args.common.sdlDeviceName);
                }

                version(dummy_output) {
                        import dummy_output : DummyOutput;
                        output = new DummyOutput();
                }
        }

        output.init();
        scope(exit) output.shutdown();

        auto morse = new Morse(output,
                               args.common.toneHz,
                               args.common.wpm,
                               args.common.charSpacingWpm,
                               args.common.wordSpacingWpm,
                               args.common.riseTimeMs / 1000.0f);

        import core.thread : Thread;
        import core.time   : msecs;
        import std.conv    : to;

        Thread.sleep(msecs(to!long(args.common.startDelay * 1000)));

        final switch(args.command) {
                case Command.test:
                        doTest(output, morse, args);
                        break;
                case Command.lesson:
                        doLesson(output, morse, args);
                        break;
                case Command.text:
                        doText(output, morse, args);
                        break;
                case Command.listdevices:
                        break;
        }

        Thread.sleep(msecs(to!long(args.common.endDelay * 1000)));

        return 0;
}
