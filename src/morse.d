module morse;

import output : Output;

class Morse
{
    public:

        this(Output output,
             int toneHz,
             float charWpm,
             float charSpacingWpm,
             float wordSpacingWpm,
             float riseTime)
        {
                _output = output;

                _dotTime = 1.2f / charWpm; // PARIS timing
                _dashTime = _dotTime * 3;
                _charSpaceTime = (1.2f / charSpacingWpm) * 3; // PARIS timing
                _wordSpaceTime = (1.2f / wordSpacingWpm) * 7; // PARIS timing

                int sr = output.getSampleRate();

                import generation : generateTone, generateSpace;

                auto dotData   = generateTone(sr, toneHz, _dotTime, riseTime);
                auto dashData  = generateTone(sr, toneHz, _dashTime, riseTime);
                auto elementSpaceData = generateSpace(sr, _dotTime);
                auto charSpaceData    = generateSpace(sr, _charSpaceTime);
                auto wordSpaceData    = generateSpace(sr, _wordSpaceTime);

                _dot          = output.addSound(dotData);
                _dash         = output.addSound(dashData);
                _elementSpace = output.addSound(elementSpaceData);
                _charSpace    = output.addSound(charSpaceData);
                _wordSpace    = output.addSound(wordSpaceData);
        }

        bool isTextValid(string text)
        {
                import morse_alphabet : alphabet;

                foreach(c; text) {
                        if(c != ' ' && !(c in alphabet)) {
                                return false;
                        }
                }

                return true;
        }

        char firstInvalidChar(string text)
        {
                import morse_alphabet : alphabet;

                foreach(c; text) {
                        if(c != ' ' && !(c in alphabet)) {
                                return c;
                        }
                }

                assert(false);
        }

        float timeText(string text)
        {
                return internalOutputText(text, true);
        }

        void outputText(string text)
        {
                internalOutputText(text, false);
        }

    private:

        float internalOutputText(string text, bool timeOnly)
        {
                import std.string    : format;
                import std.exception : enforce;

                enforce(isTextValid(text),
                        format("Invalid character: '%s'",
                                firstInvalidChar(text)));

                bool firstChar = true;
                float time = 0;

                auto outputSound = (int sound) {
                        if(!timeOnly) { _output.queueSound(sound); }
                };

                foreach(c; text) {
                        if(c == ' ') {
                                firstChar = true;
                                outputSound(_wordSpace);
                                time += _wordSpaceTime;
                                continue;
                        }

                        if(firstChar) {
                                firstChar = false;
                        } else {
                                outputSound(_charSpace);
                                time += _charSpaceTime;
                        }

                        import morse_alphabet : alphabet;

                        auto data = alphabet[c];
                        bool firstElement = true;

                        foreach(d; data) {
                                if(firstElement) {
                                        firstElement = false;
                                } else {
                                        outputSound(_elementSpace);
                                        time += _dotTime;
                                }

                                if(d == 1) {
                                        outputSound(_dot);
                                        time += _dotTime;
                                } else {
                                        outputSound(_dash);
                                        time += _dashTime;
                                }
                        }
                }

                return time;
        }

    private:
        Output _output;

        float _dotTime;
        float _dashTime;
        float _charSpaceTime;
        float _wordSpaceTime;

        int _dot;
        int _dash;
        int _elementSpace;
        int _charSpace;
        int _wordSpace;
};

