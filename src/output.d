module output;

interface Output
{
        void init();
        void shutdown();
        int addSound(immutable(float)[] data);
        void queueSound(int id);
        int getSampleRate();
        void startPlayback();
        void stopPlayback();
        void waitUntilFinished();
};
