module sdl_output;

version(sdl_output) {

import std.string        : toStringz, fromStringz, format;
import std.exception     : enforce;

import bindbc.sdl : SDL_AudioDeviceID, SDL_PauseAudioDevice, SDL_Delay,
                    SDL_GetError;

import basic_output : BasicOutput;

private void sdlEnforce(bool condition, string text)
{
        import std.format : format;
        enforce(condition, format("%s: %s", text, SDL_GetError()));
}

class SdlOutput : BasicOutput
{
    public:
        enum SampRate = 48000;

    public:
        this(string deviceName="")
        {
                super(SampRate);
                _deviceName = deviceName;
        }

        static string[] getDeviceNames()
        {
                import bindbc.sdl;

                string[] res;
                initSDL();

                foreach(i; 0..SDL_GetNumAudioDevices(false)) {
                        auto s = SDL_GetAudioDeviceName(i, false).fromStringz;
                        res ~= s.dup();
                }

                return res;
        }

        void init()
        {
                import bindbc.sdl;

                initSDL();

                SDL_AudioSpec want, have;

                want.freq = SampRate;
                want.format = AUDIO_F32;
                want.channels = 1;
                want.samples = 4096;
                want.userdata = cast(void*)this;
                want.callback = &audioCallbackProxy;

                bool good = _deviceName == "";
                foreach(i; 0..SDL_GetNumAudioDevices(0)) {
                        auto name = SDL_GetAudioDeviceName(i, 0).fromStringz();
                        if(name == _deviceName) {
                                good = true;
                                break;
                        }
                }

                enforce(good,
                        format("Cannot find audio device: '%s'", _deviceName));

                _dev = SDL_OpenAudioDevice(
                        _deviceName == ""? null : _deviceName.toStringz(),
                        0, &want, &have, 0
                );

                sdlEnforce(_dev != 0, "SDL_OpenAudioDevice error");
        }

        void shutdown()
        {
                if(_dev != 0) {
                        import bindbc.sdl;
                        SDL_CloseAudioDevice(_dev);
                        _dev = 0;
                }
        }

        void startPlayback()
        {
                SDL_PauseAudioDevice(_dev, 0);
        }

        void stopPlayback()
        {
                SDL_PauseAudioDevice(_dev, 1);
        }

        void waitUntilFinished()
        {
                while(queueLength() > 0) {
                        SDL_Delay(10);
                }
        }

    private:
        static void initSDL()
        {
                import bindbc.sdl;

                SDLSupport ret = loadSDL();
                enforce(ret == sdlSupport, "Unable to load SDL library.");

                auto initRet = SDL_Init(SDL_INIT_AUDIO);
                sdlEnforce(initRet == 0, "SDL_Init error");
        }

        extern(C) static void audioCallbackProxy(void* userdata, ubyte* stream, int len) nothrow
        {
                try {
                        SdlOutput me = cast(SdlOutput)userdata;
                        me.audioCallback(stream, len);
                } catch(Exception e) {
                        // Using the current version of dmd, the program will
                        // actually crash if an exeption is thrown anywhere
                        // underneath this function, even if it is caught
                        // correctly.
                }
        }

        void audioCallback(ubyte* stream, int len)
        {
                synchronized(_mutex) {
                        float* fstream = cast(float*)stream;
                        len /= float.sizeof;

                        fstream[0..len] = 0;
                        auto streamPos = 0;
                        auto streamLeft = len;

                        while(queueLength() > 0 && streamLeft > 0) {
                                auto tone = getSound(queueFront());
                                int toneLeft = cast(int)(tone.length - _soundPos);
                                int copyAmount = 0;
                                if(streamLeft <= toneLeft) {
                                        copyAmount = streamLeft;
                                } else {
                                        copyAmount = toneLeft;
                                }

                                fstream[streamPos..streamPos+copyAmount] =
                                        tone[_soundPos.._soundPos+copyAmount];
                                _soundPos   += copyAmount;
                                streamLeft -= copyAmount;
                                streamPos  += copyAmount;

                                if(_soundPos == tone.length) {
                                        popQueue();
                                        _soundPos = 0;
                                }
                        }
                }
        }

    private:
        string _deviceName     = null;
        SDL_AudioDeviceID _dev = 0;

        int _soundPos          = 0;
};


} // version(sdl_output) {

