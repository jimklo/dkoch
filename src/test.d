module test;

import morse         : Morse;
import output        : Output;
import args          : Arguments;
import edit_distance : editDistance, EditType, EDResult;

private string instructions =
        "Enter the characters you hear, and press <Enter> when " ~
        "the sequence is complete:\n";

public void doTest(Output output, Morse morse, Arguments* args)
{
        import std.stdio : readln;

        string test = "";

        if(args.test.wordListFile != null) {
                test = generateTest(morse,
                                    args.test.wordListFile,
                                    args.test.testTime);
        } else {
                test = generateTest(morse, 
                                    args.test.groupChars,
                                    args.test.highFreqChars,
                                    args.test.highFreqPercent,
                                    args.test.testTime,
                                    args.test.groupLengthMin,
                                    args.test.groupLengthMax);
        }

        import std.stdio : writefln, writeln;

        //debug {
        //        writefln("Test: \n%s\n", test);
        //}

        if(!args.test.quiet) {
                writeln(instructions);
        }

        morse.outputText(test);
        output.startPlayback();
        output.waitUntilFinished();

        import std.string : strip;
        auto answer = strip(readln());

        printTestResult(test, answer, !args.common.noColour);
}


private struct AnswerGroup
{
        int[2] testIdx;
        int[2] ansIdx;
}

// The logic for adjusting the output based on terminal size isn't amazing,
// so just output consistently for now.
private int[2] getTermDimensions()
{
        return [ 50, 80 ];
}

/+

private int[2] getTermDimensions()
{
        version(CRuntime_Glibc) {
                import core.sys.posix.sys.ioctl;
                import std.stdio : stdin;

                winsize ts;
                ioctl(stdin.fileno, TIOCGWINSZ, &ts);

                return [ ts.ws_row, ts.ws_col ];
        } else {
                return [ 0, 0 ];
        }
}

+/

// This is used to try to make the output more 'square', for example, if the
// terminal is very wide, try not to just print the result on a single line.
private ulong getMaxCols(ulong lineCount)
{
        auto rows = getTermDimensions[0];

        // Give a bit of space for the prompt, the total, etc
        if(rows > 6) { rows -= 6; }
        if(rows <= 1) { rows = 1; }
        auto d = rows / lineCount;
        auto r = rows % lineCount;

        return d + (r > 0? 1 : 0);
}

private ulong getMaxColWidth(ulong lineCount)
{
        import std.algorithm : max;

        return max(0, getTermDimensions()[1] - 4) / 2;
}

private AnswerGroup[] splitGroups(string test, EDResult result)
{
        int t = 0;
        int a = 0;
        string answer = result.text;
        AnswerGroup[] groups;

        while(t < test.length) {
                AnswerGroup group;
                int start = t;
                int end = t;

                while(end < test.length && test[end] != ' ') {
                        end++;
                }

                // Add the space at the end to this group, if it exists.
                if(end < test.length) {
                        end += 1;
                }

                group.testIdx = [start, end];
                t = end;

                int count = end - start;
                start = a;
                end = a;

                while(count > 0) {
                        if(result.edits[end] != EditType.Delete) {
                                count--;
                        }
                        end++;
                }

                // Add any deleted characters to this group
                if(end >= 1 && result.edits[end-1] == EditType.Delete) {
                        while(end < answer.length &&
                        result.edits[end] == EditType.Delete) {
                                end++;
                        }
                }

                group.ansIdx = [start, end];
                a = end;

                groups ~= group;
        }

        return groups;
}

private string generateTest(Morse morse,
                            string wordListFile,
                            int testTime)
{
        import std.random : uniform;
        import std.file : readText;
        import std.string : strip, splitLines, format;
        import std.exception : enforce;

        string[] words = [];
        foreach(line; readText(wordListFile).splitLines()) {
                auto l = line.strip();
                if(morse.isTextValid(l) && l.length > 0) {
                        words ~= l;
                }
        }

        enforce(words.length > 0,
                format("File '%s' contains no valid words!", wordListFile));

        string output = "";

        while(true) {
                output ~= words[uniform(0, words.length)];

                if(morse.timeText(output) >= testTime) {
                        break;
                }

                output ~= " ";
        }

        return output;
}

private string generateTest(Morse morse,
                            string groupChars,
                            string highFreqChars,
                            int highFreqPercent,
                            int testTime,
                            int groupLengthMin,
                            int groupLengthMax)
{
        import std.random : uniform;

        string output = "";

        while(true) {
                auto lenRange = groupLengthMax - groupLengthMin;
                auto len = groupLengthMin + uniform(0, lenRange + 1);
                foreach(c; 0 .. len) {
                        auto chars = groupChars;
                        if(highFreqChars != "") {
                                if(uniform(0, 100) < highFreqPercent) {
                                        chars = highFreqChars;
                                }
                        }
                        output ~= chars[uniform(0, chars.length)];
                }

                if(morse.timeText(output) >= testTime) {
                        break;
                }

                output ~= " ";
        }

        return output;
}

private string alignAnswer(string test, string answer)
{
        import std.regex;

        answer = answer.replaceAll(regex("[ ]+"), " ");
        answer = answer.replaceAll(regex("\n"), "");
        answer = answer.replaceAll(regex("[ \n]*$"), "");
        answer = answer.replaceAll(regex("^[ ]*"), "");

        if(answer.length >= test.length) {
                return answer;
        }

        // This helps makes the results more intelligible if you miss a bunch of 
        // letters at the beginning or end of the test.
        ulong bestIdx = 0;
        int bestScore = int.max;
        foreach(i; 0..(test.length - answer.length)) {

                auto result = editDistance(answer, test[i..i+answer.length]);
                int score = result.distance;

                if(score < bestScore) {
                        bestScore = score;
                        bestIdx = i;

                        // Best score possible:
                        if(score == 0) {
                                break;
                        }
                }
        }

        foreach(i; 0..bestIdx) {
                answer = " " ~ answer;
        }

        return answer;
}

private struct OutputLine
{
        char[] text;
        int displayLength;
        void reset() {
                text.length = 0;
                displayLength = 0;
        }
}

private void printOutputLines(ref OutputLine[2][] lines)
{
        import std.stdio : write, writef, writeln, writefln;
        import std.algorithm : max;

        ulong longestLeft = 0;
        foreach(line; lines) {
                longestLeft = max(longestLeft, line[0].displayLength);
        }

        ulong longestTotal = 0;

        writeln();

        foreach(line; lines) {
                writef("%s", line[0].text);
                foreach(i; 0..(longestLeft - line[0].displayLength)) {
                        write(" ");
                }
                writefln("| %s", line[1].text);

                ulong len = longestLeft + line[1].displayLength + 2;
                longestTotal = max(longestTotal, len);
        }

        foreach(i; 0..longestTotal) {
                write("-");
        }
        writeln();
}

private void printTestResult(string test, string answer, bool colour)
{
        import std.conv      : to;
        import std.stdio     : write, writeln, writef, writefln;
        import std.algorithm : min, max;

        answer = alignAnswer(test, answer);

        auto result = editDistance(answer, test);

        debug(debug_test_results) {
                import std.stdio : writef, writefln;
                writefln("Test:   -%s-", test);
                writefln("Answer: -%s-", answer);
                writefln("Edits:  -%s-", result.text);
                writef("         ");

                foreach(i; 0..result.text.length) {
                        final switch(result.edits[i]) {
                                case EditType.None: write(" "); break;
                                case EditType.Delete: write("d"); break;
                                case EditType.Substitute: write("s"); break;
                                case EditType.Insert: write("i"); break;
                        }
                }
                writeln();
        }

        auto groups = splitGroups(test, result);
        auto text = result.text.dup;

        auto red = "";
        auto green = "";
        auto reset = "";

        if(colour) {
                red   = "\033[31m";
                green = "\033[32m";
                reset = "\033[0m";
        }

        int longestTest = 0;
        int longestAns = 0;
        foreach(grp; groups) {
                longestTest = max(longestTest, grp.testIdx[1] - grp.testIdx[0]);
                longestAns = max(longestAns, grp.ansIdx[1] - grp.ansIdx[0]);
        }

        ulong maxCols = getMaxCols(groups.length * colour? 2 : 1);
        ulong maxColWidth = getMaxColWidth(groups.length * colour? 2 : 1);

        foreach(i; 0..text.length) {
                import std.uni : isWhite;
                if(result.edits[i] != EditType.None && isWhite(text[i])) {
                        text[i] = '_';
                }
        }

        int errors = min(result.distance, test.length);
        float score = 1.0f - (to!float(errors) / test.length);

        int longestLine = 0;

        OutputLine[2][] outLines;
        OutputLine[2] current;
        OutputLine[2] marks;

        int col = 0;
        foreach(grp; groups) {

                char[] newLeft;
                char[] newRight;
                int newLeftLen = 0;
                int newRightLen = 0;
                char[] leftMarks;
                char[] rightMarks;

                int t = grp.testIdx[0];
                foreach(a; grp.ansIdx[0]..grp.ansIdx[1]) {
                        auto et = result.edits[a];

                        auto mark = "";

                        if(et == EditType.None) {
                                newLeft ~= green;
                                newRight ~= green;
                                mark = " ";
                        } else {
                                newLeft ~= red;
                                newRight ~= red;
                                mark = "^";
                        }

                        rightMarks ~= mark;
                        if(et == EditType.Insert) {
                                newRight ~= '_';
                                newRightLen++;
                        } else {
                                newRight ~= text[a];
                                newRightLen++;
                        }

                        if(result.edits[a] != EditType.Delete) {
                                leftMarks ~= mark;
                                newLeft ~= test[t++];
                                newLeftLen++;
                        }
                }

                newLeft  ~= reset;
                newRight ~= reset;

                col++;

                bool nextLine = 
                        col >= maxCols || 
                        current[0].displayLength + newLeftLen > maxColWidth ||
                        current[1].displayLength + newRightLen > maxColWidth;

                if(current[0].displayLength == 0 
                && current[1].displayLength == 0) {
                        nextLine = false;
                }

                if(nextLine) {
                        outLines ~= current;
                        if(!colour) { outLines ~= marks; }
                        current[0].reset();
                        current[1].reset();
                        marks[0].reset();
                        marks[1].reset();
                        col = 0;
                }

                current[0].text ~= newLeft;
                current[0].displayLength += newLeftLen;
                current[1].text ~= newRight;
                current[1].displayLength += newRightLen;
                marks[0].text ~= leftMarks;
                marks[0].displayLength += newLeftLen;
                marks[1].text ~= rightMarks;
                marks[1].displayLength += newRightLen;
        }

        if(current[0].displayLength > 0 || current[1].displayLength > 0) {
                outLines ~= current;
                if(!colour) { outLines ~= marks; }
        }

        printOutputLines(outLines);

        writefln("Errors: %s", result.distance);
        writefln("Score: %s%0.1f%%%s", score > .9? green : red,
                                     score * 100.0f,
                                     reset);
}

